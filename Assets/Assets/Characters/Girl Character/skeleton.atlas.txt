
skeleton.png
size: 512,64
format: RGBA8888
filter: Linear,Linear
repeat: none
source/donutCharacter_hand
  rotate: true
  xy: 206, 8
  size: 12, 24
  orig: 12, 24
  offset: 0, 0
  index: -1
source/donutCharacter_head
  rotate: false
  xy: 2, 2
  size: 100, 47
  orig: 100, 47
  offset: 0, 0
  index: -1
source/donutCharacter_lowerBody
  rotate: false
  xy: 206, 22
  size: 76, 27
  orig: 76, 27
  offset: 0, 0
  index: -1
source/donutCharacter_upperBody
  rotate: false
  xy: 104, 2
  size: 100, 47
  orig: 100, 47
  offset: 0, 0
  index: -1
