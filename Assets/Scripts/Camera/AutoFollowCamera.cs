﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class AutoFollowCamera : MonoBehaviour
{
    private GameObject _gui;
    private Character _lastTarget;
    private UI _ui;
    [SerializeField]
    private Vector3 offset;

    void Start()
    {
        _gui = Managers.Get("GUI");
        _ui = _gui.GetComponentInChildren<UI>();
        KillOtherCameras();
        StartCoroutine(TrackActivePlayer());
    }

    void OnLevelWasLoaded()
    {
        KillOtherCameras();
    }

    private void KillOtherCameras()
    {
        var otherCameras = FindObjectsOfType<Camera>()
            .Where(c => !c.name.Contains("GameCamera"))
            .Where(c => c != null);

        foreach(var camera in otherCameras)
        {
            Destroy(camera.gameObject);
        }
    }

    IEnumerator TrackActivePlayer()
    {
        do
        {
            yield return null;
        } while ((_lastTarget = _ui.ActiveCharacter) == null);

        while (_lastTarget != null)
        {
            if (_lastTarget != _ui.ActiveCharacter)
            {
                _lastTarget = _ui.ActiveCharacter;
                yield return StartCoroutine(LerpToNewTarget(_ui.ActiveCharacter.transform));
            }
            transform.position = _lastTarget.transform.position + offset;
            yield return null;
        }
        StartCoroutine(TrackActivePlayer());
    }

    private IEnumerator LerpToNewTarget(Transform target)
    {
        Vector3 startPosition = transform.position + offset;

        float time = 0;
        while ((time += Time.deltaTime) < 1)
        {
            transform.position = Vector3.Lerp(startPosition, target.position + offset, time);
            yield return null;
        }
        transform.position = target.position;
    }
}
