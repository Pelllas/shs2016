﻿using Interfaces;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterMovement : MonoBehaviour, ITimeModifiable
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private MoveDirection activeDirection = MoveDirection.None;
    [SerializeField]
    private Rigidbody2D body;
    private List<KeyValuePair<object, float>> timeModifications = new List<KeyValuePair<object, float>>();
    [SerializeField]
    private float currentTimeFactor = 1;
    private Vector3 baseScale;

    void Awake()
    {
        activeDirection = MoveDirection.None;
        baseScale = transform.localScale;
    }

    void Update()
    {
        MovementLogic();
    }

    private void MovementLogic()
    {
        Vector2 direction = Vector2.zero;
        switch (activeDirection)
        {
            case MoveDirection.Left:
                direction = Vector2.left;
                transform.localScale = new Vector3(-baseScale.x, baseScale.y, baseScale.z);
                break;
            case MoveDirection.Up:
                direction = Vector2.up;
                break;
            case MoveDirection.Right:
                direction = Vector2.right;
                transform.localScale = baseScale;
                break;
            case MoveDirection.Down:
                direction = Vector2.down;
                break;
            case MoveDirection.None:
                direction = Vector2.zero;
                break;
        }
        body.velocity = direction * speed * currentTimeFactor;
    }

    public void SetDirection(MoveDirection moveDirection)
    {
        activeDirection = moveDirection;
    }

    public void AddModification(object blocker, float factor)
    {
        timeModifications.Add(new KeyValuePair<object, float>(blocker, factor));
        currentTimeFactor = CurrentTimeFactor();
    }

    public void RemoveModification(object blocker, float factor)
    {
        // Todo: Find more elegant solution
        var index = timeModifications
            .FindIndex(kvp => kvp.Key == blocker);

        timeModifications.RemoveAt(index);
        currentTimeFactor = CurrentTimeFactor();
    }

    // 0 object modifying yields a modification of 1 (no change)
    // 1 object modifying with factor 0.5 yields a factor of 0.5 (half speed)
    // 2 objects modifying with factors 1.2 and 0.8 yields a modification of 1
    // 3 objects modifying with factors 0.9, 0.9, 0.9 yields a modification of 0.7
    private float CurrentTimeFactor()
    {
        return 1 - timeModifications
            .Select(kvp => 1 - kvp.Value)
            .Sum();
    }
}
