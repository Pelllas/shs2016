﻿namespace Interfaces
{
    public interface IDamageable
    {
        int ReceiveDamage(int damage); 
    }
}
