﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IJsonSerializable
    {
        IDictionary<string, object> Serialize();
    }
}
