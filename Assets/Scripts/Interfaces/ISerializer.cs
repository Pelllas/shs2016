﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface ISerializer
    {
        void Register(string key, IJsonSerializable newObject);
        void Deregister(string key);
        IDictionary<string, object> Serialize(string key, IJsonSerializable serializableObject);
        IDictionary<string, object> Serialize(KeyValuePair<string, IJsonSerializable> serializableObject);
        IDictionary<string, object> Serialize(IEnumerable<KeyValuePair<string, IJsonSerializable>> serializableObjects);
        IDictionary<string, object> SaveRegisteredObjects();
        T Deserialize<T>(string key) where T : class, IJsonSerializable;
    }
}
