﻿namespace Interfaces
{
    public interface ISetActiveState
    {
        void SetActiveState(bool newState);
    }
}