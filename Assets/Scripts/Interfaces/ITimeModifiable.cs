﻿namespace Interfaces
{
    public interface ITimeModifiable
    {
        void AddModification(object blocker, float factor);
        void RemoveModification(object blocker, float factor);
    }
}