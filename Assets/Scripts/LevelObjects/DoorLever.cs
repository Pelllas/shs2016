﻿using UnityEngine;
using Interfaces;
using LevelObjects.BaseClasses;
using Utility;

namespace LevelObjects
{
    public class DoorLever : MonoBehaviour, IInteractableObject
    {
        [SerializeField]
        private Door[] Doors;

        [SerializeField]
        private bool isPulled = false;

        [SerializeField]
        private float cooldown = 5;

        private bool available = true;

        public void Interact()
        {
            available = false;
            foreach (var door in Doors)
            {
                door.Interact();
            }
            var timer = new Timer(cooldown, ReEnable);
        }

        private void ReEnable()
        {
            available = true;
        }
    }
}
