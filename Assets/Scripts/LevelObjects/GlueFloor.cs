﻿using UnityEngine;

namespace LevelObjects
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class GlueFloor : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D collider)
        {
            var component = collider.GetComponent<CharacterMovement>();
            if(component != null)
            {
                component.AddModification(this, 0.2f);
            }
        }

        void OnTriggerExit2D(Collider2D collider)
        {
            var component = collider.GetComponent<CharacterMovement>();
            if (component != null)
            {
                component.RemoveModification(this, 0.2f);
            }
        }
    }
}
