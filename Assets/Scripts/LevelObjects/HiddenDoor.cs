﻿using UnityEngine;
using System.Collections;
using LevelObjects.BaseClasses;

namespace LevelObjects
{
    public class HiddenDoor : Door
    {
        [SerializeField]
        private Vector2 endPos;

        [SerializeField]
        private Transform endObj;

        [SerializeField]
        private float moveSpeed;

        private Vector2 startPos;
        private Vector2 nextDestination;
        private bool isMoving = false;
        private bool isOpen = false;

        void Start()
        {
            startPos = transform.position;
            endPos = endObj.transform.position;
        }

        IEnumerator TranslatePosition(Vector3 finalPosition)
        {
            Vector2 startPosition = transform.position;
            isMoving = true;
            while(transform.position != finalPosition)
            {
                transform.position = Vector2.Lerp(startPosition, finalPosition, moveSpeed * Time.fixedDeltaTime);
                yield return null;
            }
            isOpen = !isOpen;
            isMoving = false;
            nextDestination = startPosition;
        }

        public override void Interact()
        {
            if (!isMoving)
            {
                StartCoroutine(TranslatePosition(nextDestination));
            }
        }
    }
}
