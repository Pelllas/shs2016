﻿using System;
using Interfaces;
using UnityEngine;
using System.Linq;

public class Lever : MonoBehaviour, IInteractableObject
{
    private bool leverPlacement = true;

    [SerializeField]
    GameObject leverToRotate;

    [SerializeField]
    private float rotationAngle = 5;

    [SerializeField]
    private GameObject[] objects;

    private IInteractableObject[] interactableObjects;

    [SerializeField]
    private bool isPulled = false;

    [SerializeField]
    private bool withinRange = false;

    void Awake()
    {
        interactableObjects = objects
            .Select(o => o.GetComponent<IInteractableObject>())
            .Where(c => c != null)
            .ToArray();
    }

    public void Interact()
    {
        isPulled = true;

        foreach (var interactableObject in interactableObjects)
        {
            interactableObject.Interact();
        }
        RotateLever(leverToRotate);
    }

    private void RotateLever(GameObject leverToRotate)
    {
        leverPlacement = !leverPlacement;
        
        if (leverPlacement == true)
        {
            leverToRotate.transform.Rotate(new Vector3(0, 0, rotationAngle));
        }
        else
        {
            leverToRotate.transform.Rotate(new Vector3(0, 0, -rotationAngle));
        }
    }
}
