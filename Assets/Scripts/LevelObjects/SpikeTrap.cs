﻿using Interfaces;
using UnityEngine;

public class SpikeTrap : MonoBehaviour, ISetActiveState
{
    SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void SetActiveState(bool newState)
    {
        spriteRenderer.enabled = newState;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (spriteRenderer.enabled)
        {
            var component = collider.GetComponent<IKillable>();
            if (component != null)
            {
                component.Kill();
            }
        }
    }
}
