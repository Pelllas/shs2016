﻿using System;
using Interfaces;
using UnityEngine;

public class Projectile : MonoBehaviour, ITimeModifiable
{
    [SerializeField]
    private Rigidbody2D body;
    private Vector3 direction;
    public float speed = 5;
	
	void Update ()
    {
        body.velocity = direction * speed;
	}

    public void SetDirection(Vector3 dir)
    {
        direction = dir.normalized;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var component = collider.GetComponent<IKillable>();
        if (component != null)
        {
            component.Kill();
        }
    }

    public void AddModification(object blocker, float factor)
    {
        speed *= factor;
    }

    public void RemoveModification(object blocker, float factor)
    {
        speed /= factor;
    }
}
