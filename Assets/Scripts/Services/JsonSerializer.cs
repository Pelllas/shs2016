﻿using Interfaces;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Services
{
    public class JsonSerializer : MonoBehaviour, ISerializer
    {
        private readonly IDictionary<string, IJsonSerializable> objects = new Dictionary<string, IJsonSerializable>();
        
        void Awake()
        {
            // Read from file.
        }

        public void Register(string key, IJsonSerializable newObject)
        {
            objects.Add(key, newObject);
        }

        public void Deregister(string key)
        {
            objects.Remove(key);
        }

        public IDictionary<string, object> SaveRegisteredObjects()
        {
            return Serialize(objects);
        }

        public IDictionary<string, object> Serialize(IEnumerable<KeyValuePair<string, IJsonSerializable>> serializableObjects)
        {
            return serializableObjects.ToDictionary(
                kvp => kvp.Key,
                kvp => (object)kvp.Value.Serialize());
        }

        public IDictionary<string, object> Serialize(KeyValuePair<string, IJsonSerializable> serializableObject)
        {
            return Serialize(serializableObject.Key, serializableObject.Value);
        }

        public IDictionary<string, object> Serialize(string key, IJsonSerializable serializableObject)
        {
            return new Dictionary<string, object>
            {
                { key, serializableObject.Serialize() }
            };
        }

        public T Deserialize<T>(string key) where T : class, IJsonSerializable
        {
            IJsonSerializable result;
            return objects.TryGetValue(key, out result)
                ? result as T
                : default(T);
        }
    }
}
