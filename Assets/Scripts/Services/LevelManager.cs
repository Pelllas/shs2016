﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utility;

namespace Services
{
    [RequireComponent(typeof(AudioSource))]
    public class LevelManager : MonoBehaviour
    {
        bool busy = false;
        AudioSource audioSource;

        void Awake()
        {
            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }
        }

        public void LoadNextScene()
        {
            if (!busy)
            {
                busy = true;
                PlaySound();
                float length = 0;
                if(audioSource != null && audioSource.clip != null)
                {
                    length = audioSource.clip.length;
                }
                if(length == 0)
                {
                    ChangeLevel();
                }
                else
                {
                    new Timer(length, ChangeLevel);
                }
            }
        }

        private void ChangeLevel()
        {
            LogHelper.Log("Loading level {0}", NextLevel, DebugLevel.All);
            SceneManager.LoadScene(NextLevel);
            busy = false;
        }

        public void RestartLevel()
        {
            OnLevelRestart();
            SceneManager.LoadScene(CurrentLevel);
        }

        private int CurrentLevel
        {
            get
            {
                return SceneManager.GetActiveScene().buildIndex;
            }
        }

        private int NextLevel
        {
            get
            {
                return CurrentLevel + 1;
            }
        }

        public Action OnLevelRestart { get; set; }

        private void PlaySound()
        {
            audioSource.Play();
        }
    }
}
