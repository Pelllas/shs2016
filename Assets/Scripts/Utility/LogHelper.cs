﻿using UnityEngine;

namespace Utility
{
    public class LogHelper
    {
        public static void Log(string message, object format = null, DebugLevel debugLevel = DebugLevel.All)
        {
            if (Application.platform == RuntimePlatform.Android || 
                Application.platform == RuntimePlatform.IPhonePlayer)
            {
                return;
            }

            if (Boot.DEBUG_LEVEL >= debugLevel)
            {
                if (format != null)
                {
                    Debug.Log(string.Format(message, format));
                }
                else
                {
                    Debug.Log(message);
                }
            }
        }
    }
}
